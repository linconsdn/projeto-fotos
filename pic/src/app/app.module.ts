import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PhotosMudule } from './photos/photos.mudules';




@NgModule({
  declarations: [
    AppComponent 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PhotosMudule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
