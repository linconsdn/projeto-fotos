import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import {NgModule} from "@angular/core"
import { PhotoModule } from './photo/photo.module';
import { PhotosFormModule } from './photos-form/photos-form.module'
import { PhotosListModule } from './photos-list/photo-list.module'


@NgModule({
    imports: [
        PhotoModule,
        PhotosFormModule,
        PhotosListModule,
        HttpClientModule,
        CommonModule    
    ]
})
export class PhotosMudule{}