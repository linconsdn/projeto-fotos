import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html'
})
export class SeachComponent{
    
    @Output() onTyping = new EventEmitter<string>();
    @Input() value: string = '';
    debounce: Subject<string> = new Subject<string>();


    onKeyUp(target : any) {
        if(target instanceof EventTarget) {
          var elemento = target as HTMLInputElement;
          this.debounce.next(elemento.value);
          this.debounce
            .pipe(debounceTime(300))
            .subscribe(filter => this.onTyping.emit(filter))
        }
      }
}